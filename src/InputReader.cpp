//============================================================================
// Create on   : May 9, 2017
// Name        : InputReader.cpp
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
// Copyright   : Distributed under the terms of the GNU GPL v3
//============================================================================

#include "InputReader.h"

#include <fstream>

void InputReader::readFromFile(const std::string& filePath,
		Point3HVec& points)
{
	std::ifstream inputFile(filePath.c_str());
	if (!inputFile.is_open())
	{
		std::cerr << "Dosya okumak için açılamadı.";
		std::exit(EXIT_FAILURE);
	}

	RealType p[3];
	while(inputFile >> p[0] >> p[1] >> p[2])
	{
		points.push_back({ p[0], p[1], p[2] });
	}

	inputFile.close();
}

void InputReader::readFromFile(const std::string& filePath, Point3HVec& points,
		const unsigned long pointNum)
{
	std::ifstream inputFile(filePath.c_str());
	if (!inputFile.is_open())
	{
		std::cerr << "Dosya okumak için açılamadı.";
		std::exit(EXIT_FAILURE);
	}

	unsigned long count = 0;
	RealType p[3];
	while(count++ != pointNum)
	{
		inputFile >> p[0] >> p[1] >> p[2];
		points.push_back({ p[0], p[1], p[2] });
	}

	inputFile.close();
}

void InputReader::readFromFile(const std::string& filePath, Point3HVec& points,
		const unsigned long begin, const unsigned long end)
{
	std::ifstream inputFile(filePath.c_str());
	if (!inputFile.is_open())
	{
		std::cerr << "Dosya okumak için açılamadı.";
		std::exit(EXIT_FAILURE);
	}

	inputFile.seekg(begin); // Okumayı gerekli konumdan başlat.

	RealType p[3];
	for (unsigned long i = begin; i < end; ++i)
	{
		inputFile >> p[0] >> p[1] >> p[2];
		points.push_back({ p[0], p[1], p[2] });
	}

	inputFile.close();
}

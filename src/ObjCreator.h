//============================================================================
// Create on   : May 8, 2017
// Name        : ObjCreator.h
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
//============================================================================

#ifndef OBJCREATOR_H_
#define OBJCREATOR_H_

#include "gDel3D/CommonTypes.h"

const std::string VERTEX_KEYWORD  = "v"; // Obj formatında geometrik bir vertexi temsil eder.
const std::string FACE_KEYWORD    = "f"; // Obj formatında bir nesnenin yüzünü temsil eder.
const std::string COMMENT_KEYWORD = "#"; // Obj formatında yorum bırakmak için kullanılır.

//============================================================================
// Üretilen tetrahedronun 4 köşe noktasını kullanarak 4 yüzü oluşturan
// üçgenleri elde eder ve .obj formatında verilen dosyaya yazmak için kullanır.
// @author Ender Yıldırım
// @since May 8, 2017
//============================================================================
class ObjCreator
{
private:

	template <typename T>
	static std::string NumberToString ( T Number )
	{
		std::ostringstream ss;
		ss << Number;
		return ss.str();
	}

public:

	//============================================================================
	// Verilen nokta sayısını bölümlere ayırarak obj dosyaları ve bilgilendirme
	// yorumlarını oluşturur.
	// @param pointNum Nokta sayısı
	// @param divide Bölümlerin sayısı
	//============================================================================
	static std::vector<std::string> createObjFiles(const int pointNum,
			const int divide);

	//============================================================================
	// Verilen dosyaya üçgenleri .obj formatında yazar. Verilan dosyanın
	// uzantısı .obj olmak zorundadır.
	// @param filePath Yazılacak dosya
	// @param pointVec Noktaları içeren vektör
	// @param output Üçgenleştirme sonucu
	// @see  http://paulbourke.net/dataformats/obj/
	//============================================================================
	static void writeToObj(const std::string& filePath,
			const Point3HVec& pointVec, const GDelOutput& output);

};

#endif /* OBJCREATOR_H_ */


//============================================================================
// Create on   : May 7, 2017
// Name        : TriangleCreator.h
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
//============================================================================

#ifndef TRIANGLECREATOR_H_
#define TRIANGLECREATOR_H_

#include <set>
#include <ostream>
#include <sstream>

#include "DelaunayChecker.h"
#include "gDel3D/CommonTypes.h"
#include "gDel3D/CPU/CPUDecl.h"



//============================================================================
// 3-Boyutlu uzayda bir üçgeni 3 farklı köşe noktasıyla temsil eder.
// @author Ender Yıldırım
// @see gDel3D/CPU/CPUDecl.h Tri structure
// @since May 8, 2017
// @deprecated Kullanılmasına gerek kalmadı, obj formatındaki dosya boyutunu
// gereksiz olarak artırıyordu.
//============================================================================
struct Triangle
{
	Point3 p[3];

	//============================================================================
	// Verilen output' a üçgeni .obj formatında yazar.
	// @param os Output
	// @see ObjCreator
	//============================================================================
	void printObjFormat(std::ostream& os) const
	{
		for (int i = 0; i < 3; ++i)
		{
			os << "v" << " " << p[i]._p[0] << " " <<  p[i]._p[1] << " " << p[i]._p[2] << "\n";
		}
	}


};

//============================================================================
// Tetrahedronlardan üçgenleri elde etmek için kullanılır.
// @author Ender Yıldırım
// @since May 8, 2017
// @depracated Kullanılmasına gerek kalmadı, obj formatında dosya oluşturken
// doğrudan tetrahedronlardan yararlanıldı ve bu sınıfın kullanılmasına
// ihtiyaç kalmadı.
//============================================================================
class TriangleCreator
{
private:

public:

	//============================================================================
	// Tetrahedron'lar için noktaları sıralayarak üçgen köşelerini oluşturur.
	// Bir tetrahedron 4 yüzden oluşur(TetFaceNum).
	// @param t tetrahedron
	// @param triArr tetrahedron köşeleri için TetFaceNum uzunluğunda dizi
	// @see http://mathworld.wolfram.com/RegularTetrahedron.html
	// @see https://en.wikipedia.org/wiki/Tetrahedron
	// @see DelaunayChecker.h TetFace
	// @see DelaunayChecker.h TetFaceNum
	//============================================================================
	static void getTetraTriArray(const Tet& t, Tri* triArr);

	//============================================================================
	// Tetrahedronun içerdiği 4 üçgeni diziye ekler. Dizi boyutu 4 olmalıdır.
	// TetFaceNum değeri bu yüzden kullanılmıştır.
	// @param t tetrahedron
	// @param triangleArr Üçgenlerin ekleneceği dizi
	//============================================================================
	static void getTetraTriangles(const Point3HVec& pointVec, const Tri* triArr,
			Triangle* triangleArr);

	//============================================================================
	// GpuDel tarafından üretilen output'daki tetrahedronları kullanarak üçgen-
	// leri elde eder.
	// @param output GpuDel tarafından üretilen output
	// @param triVec Üçgenlerin ekleneceği vektör
	// @see GpuDelaunay.h GpuDel class
	//============================================================================
	static void getTriangles(const Point3HVec* pointVec,
			const GDelOutput* output, std::vector<Triangle>* triVec);
};

#endif /*  TRIANGLECREATOR_H_ */


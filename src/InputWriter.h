//============================================================================
// Create on   : May 9, 2017
// Name        : InputWriter.h
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
// Copyright   : Distributed under the terms of the GNU GPL v3
//============================================================================

#ifndef INPUTWRITER_H_
#define INPUTWRITER_H_

#include "gDel3D/CommonTypes.h"

//============================================================================
// Rastgele üretilen noktaları dosyaya yazmak için kullanılır.
// @author Ender Yıldırım
// @since May 9, 2017
//============================================================================
class InputWriter
{
public:

	//============================================================================
	// Rastgele üretilmiş noktaları bulunduran vektör içeriğini dosyaya yazar.
	// Noktaların bulunduğu vektör bellekte yer aldığı için bellek boyutu ile
	// sınırlıdır.
	// @param filePath Yazılacak dosya
	// @param pointVec Noktaları içeren vektör
	// @exception
	//============================================================================
	static void writeToFile(const std::string& filePath,
			const Point3HVec& pointVec);

	//============================================================================
	// Verilen nokta sayısı kadar noktayı, rastgele olarak verilen min-max
	// aralığında üreterek dosyaya yazar. Noktaları bellekte tutmadığı için çok
	// büyük sayılarda nokta sayısını bile dosyaya yazabilir. Noktalar bellekte
	// yer almadığından sabit disk boyutu ile sınırlıdır.
	// @param filePath Yazılacak dosya
	// @param pointNum Yazılacak nokta sayısı
	// @param min Minimum sınır
	// @param max Maximum sınır
	//============================================================================
	static void writeToFile(const std::string& filePath, const long pointNum,
			const int min, const int max);
};

#endif /* INPUTWRITER_H_ */

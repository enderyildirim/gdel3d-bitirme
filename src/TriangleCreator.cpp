
//============================================================================
// Create on   : May 8, 2017
// Name        : TriangleCreator.cpp
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
//============================================================================

#include "TriangleCreator.h"

void TriangleCreator::getTetraTriArray(const Tet& t, Tri* triArr)
{
	for ( int i = 0; i < TetFaceNum; ++i )
	{
		// Tri vertices
		Tri tri = { t._v[ TetFace[i][0] ], t._v[ TetFace[i][1] ], t._v[ TetFace[i][2] ] };

		// Sort
		if ( tri._v[0] > tri._v[1] ) std::swap( tri._v[0], tri._v[1] );
		if ( tri._v[1] > tri._v[2] ) std::swap( tri._v[1], tri._v[2] );
		if ( tri._v[0] > tri._v[1] ) std::swap( tri._v[0], tri._v[1] );

		// Add triangle
		triArr[ i ] = tri;
	}
}

/*void TriangleCreator::getTriSet(const GDelOutput* output,
		std::set<Tri>* triSet)
{
	const TetHVec& tetVec       = output->tetVec;
	const CharHVec& tetInfoVec  = output->tetInfoVec;

	// Read triangles
	Tri triArr[ TetFaceNum ];

	for ( int ti = 0; ti < tetVec.size(); ++ti )
	{
		if ( !isTetAlive( tetInfoVec[ti] ) ) continue;

		const Tet& tet = tetVec[ ti ];

		getTetraTriArray( tet, triArr );

		triSet->insert( triArr, triArr + TetFaceNum );
	}
}*/


void TriangleCreator::getTetraTriangles(const Point3HVec& pointVec, const Tri* triArr,
		Triangle* triangleArr)
{
	for (int i = 0; i < TetFaceNum; ++i)
	{
		triangleArr[i].p[0] = pointVec[triArr[i]._v[0]];
		triangleArr[i].p[1] = pointVec[triArr[i]._v[1]];
		triangleArr[i].p[2] = pointVec[triArr[i]._v[2]];
	}
}

void TriangleCreator::getTriangles(const Point3HVec* pointVec,
		const GDelOutput* output, std::vector<Triangle>* triVec)
{
	const TetHVec& tetVec       = output->tetVec;
	const CharHVec& tetInfoVec  = output->tetInfoVec;

	Triangle triangleArr[TetFaceNum]; // Tetrahedronun 4 üçgen yüzü
	Tri triArr[TetFaceNum];
	for (int i = 0; i < tetVec.size(); ++i)
	{
		if ( !isTetAlive( tetInfoVec[i] ) ) continue;
		const Tet& tet = tetVec[ i ];
		getTetraTriArray(tet, triArr);

		getTetraTriangles(*pointVec, triArr, triangleArr);

		for (int j = 0; j < TetFaceNum; ++j)
		{
			triVec->push_back(triangleArr[j]);
		}

	}

}



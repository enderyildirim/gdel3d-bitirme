
#pragma once

#include <set>

#include "gDel3D/CommonTypes.h"
#include "gDel3D/CPU/PredWrapper.h"

class DelaunayChecker 
{
private: 
	Point3HVec*   _pointVec; 
    GDelOutput*   _output; 

	PredWrapper  _predWrapper; 

    int getVertexCount();
    int getSegmentCount();
    int getTetraCount();
    int getTriangleCount();
    void printTetraAndOpp( int ti, const Tet& tet, const TetOpp& opp );

public: 
    DelaunayChecker( Point3HVec* pointVec, GDelOutput* output ); 

    void checkEuler();
    void checkAdjacency();
    void checkOrientation();
    bool checkDelaunay( bool writeFile = false );
}; 

struct Segment
{
    int _v[2];

    inline bool equal( const Segment& seg ) const
    {
        return ( ( _v[0] == seg._v[0] ) && ( _v[1] == seg._v[1] ) );
    }

    inline bool operator == ( const Segment& seg ) const
    {
        return equal( seg );
    }

    inline bool lessThan( const Segment& seg ) const
    {
        if ( _v[0] < seg._v[0] )
            return true;
        if ( _v[0] > seg._v[0] )
            return false;
        if ( _v[1] < seg._v[1] )
            return true;

        return false; 
    }

    inline bool operator < ( const Segment& seg ) const
    {
        return lessThan( seg );
    }
};

const int TetSegNum = 6;
const int TetSeg[ TetSegNum ][2] = {
    { 0, 1 },
    { 0, 2 },
    { 0, 3 },
    { 1, 2 },
    { 1, 3 },
    { 2, 3 },
};

// Tetrahedronun 4 yüzü vardır. @see: http://mathworld.wolfram.com/RegularTetrahedron.html
const int TetFaceNum = 4;
// Bir tetrahedra'nın yüzlerini temsil ediyor. 0, 1, 2, 3 noktaları tetrahedra
// nın köşe numaraları olarak temsil edilmiş.
const int TetFace[ TetFaceNum ][3]  = {
    { 0, 1, 2 },
    { 0, 1, 3 },
    { 0, 2, 3 },
    { 1, 2, 3 },
};


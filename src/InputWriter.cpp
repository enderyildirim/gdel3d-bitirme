//============================================================================
// Create on   : May 9, 2017
// Name        : InputWriter.cpp
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
// Copyright   : Distributed under the terms of the GNU GPL v3
//============================================================================

#include <fstream>

#include "InputWriter.h"

#include "RandGen.h"

void InputWriter::writeToFile(const std::string& filePath,
		const Point3HVec& pointVec)
{
	std::ofstream outputFile(filePath.c_str());
	if (!outputFile.is_open())
	{
		std::cerr << "Dosya yazmak için açılamadı.";
		std::exit(EXIT_FAILURE);
	}

	for (int i = 0; i < pointVec.size(); ++i)
	{
		outputFile << pointVec[i]._p[0] << " " << pointVec[i]._p[1] << " " << pointVec[i]._p[2] << "\n";
	}

	outputFile.close();
}

void InputWriter::writeToFile(const std::string& filePath, const long pointNum,
		const int min, const int max)
{
	std::ofstream outputFile(filePath.c_str());
	if (!outputFile.is_open())
	{
		std::cerr << "Dosya yazmak için açılamadı.";
		std::exit(EXIT_FAILURE);
	}

	const int seed= 123456789;
	RandGen rand;
	rand.init(seed, min, max);
	for (int i = 0; i < pointNum; ++i)
	{
		Point3 point = { rand.getNext(), rand.getNext(), rand.getNext() };
		outputFile << point._p[0] << " " << point._p[1] << " " << point._p[2] << "\n";
	}

	outputFile.close();
}

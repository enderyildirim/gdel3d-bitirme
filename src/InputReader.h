//============================================================================
// Create on   : May 9, 2017
// Name        : InputReader.h
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
// Copyright   : Distributed under the terms of the GNU GPL v3
//============================================================================

#ifndef INPUTREADER_H_
#define INPUTREADER_H_

#include "gDel3D/CommonTypes.h"

//============================================================================
// Dosyada bulunan noktaları okumak için kullanılır.
// @author Ender Yıldırım
// @since May 9, 2017
//============================================================================
class InputReader
{
public:

	//============================================================================
	// Dosyadan okunan noktaları verilen vektöre ekler.
	// @param filePath Okunacak dosya
	// @param points Noktaların ekleneceği vektör
	//============================================================================
	static void readFromFile(const std::string& filePath, Point3HVec& points);

	//============================================================================
	// Verilen sayı kadar dosyadan okunan noktaları verilen vektöre ekler.
	// @param filePath Okunacak dosya
	// @param points Noktaların ekleneceği vektör
	// @param pointNum Dosyadan okunacak nokta sayısı
	//============================================================================
	static void readFromFile(const std::string& filePath, Point3HVec& points,
			const unsigned long pointNum);

	//============================================================================
	// Dosyadan verilen satır aralığı kadar noktayı okur ve verilen vektöre
	// ekler.
	// @param filePath Okunacak dosya
	// @param points Noktaların ekleneceği vektör
	// @param begin Başlangıç satırı
	// @param end Bitiş satırı
	//============================================================================
	static void readFromFile(const std::string& filePath, Point3HVec& points,
			const unsigned long begin, const unsigned long end);
};

#endif /* INPUTREADER_H_ */


#pragma once

#include "gDel3D/CommonTypes.h"
#include "RandGen.h"

enum Distribution
{
    UniformDistribution,
    GaussianDistribution,
    BallDistribution,
    SphereDistribution,
    GridDistribution,
	ThinSphereDistribution
};

const std::string DistStr[] =
{
    "Uniform",
    "Gaussian",
    "Ball",
    "Sphere",
    "Grid", 
    "ThinSphere"
};

class InputCreator 
{
private:
    RandGen _randGen; 

    void randSpherePoint( 
        RealType    radius, 
        RealType&   x, 
        RealType&   y, 
        RealType&   z 
        );

public: 
    void makePoints( 
        int             pointNum, 
        Distribution    dist,
        Point3HVec&     pointVec,
        int             seed = 0
        );
    int readPoints( 
        std::string inFilename, 
        Point3HVec& pointVec
        );
}; 


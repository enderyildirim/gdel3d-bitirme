
#pragma once

#include "GPU/CudaWrapper.h"

#ifdef _WIN32

#define NOMINMAX
#include <windows.h>

struct PerfTimer
{
    float         _freq;
    LARGE_INTEGER _startTime;
    LARGE_INTEGER _stopTime;
    long long _leftover; 
    long long _value; 

    PerfTimer()
    {
        LARGE_INTEGER freq;
        QueryPerformanceFrequency(&freq);
        _freq = 1.0f / freq.QuadPart;

        _leftover = 0; 
    }

    void start()
    {
        QueryPerformanceCounter(&_startTime);
    }

    void stop()
    {
        QueryPerformanceCounter(&_stopTime);

        _value      = _leftover + (_stopTime.QuadPart - _startTime.QuadPart); 
        _leftover   = 0; 
    }

    void pause() 
    {
        QueryPerformanceCounter(&_stopTime);
        
        _leftover += (_stopTime.QuadPart - _startTime.QuadPart); 
    }

    double value() const
    {
        return _value * _freq * 1000;
    }
};

#else

#include <sys/time.h>

const long long NANO_PER_SEC = 1000000000LL;
const long long MICRO_TO_NANO = 1000LL;

struct PerfTimer
{
    long long _startTime;
    long long _stopTime;
    long long _leftover; 
    long long _value;

    long long _getTime()
    {
        struct timeval tv;
        long long ntime;

        if (0 == gettimeofday(&tv, NULL))
        {
            ntime  = NANO_PER_SEC;
            ntime *= tv.tv_sec;
            ntime += tv.tv_usec * MICRO_TO_NANO;
        }

        return ntime;
    }

    void start()
    {
        _startTime = _getTime();
    }

    void stop()
    {
        _stopTime = _getTime();
        _value      = _leftover + _stopTime - _startTime; 
        _leftover   = 0; 
    }

    void pause()
    {
        _stopTime   = _getTime();
        _leftover   += _stopTime - _startTime;         
    }

    double value() const
    {
        return ((double) _value) / NANO_PER_SEC * 1000;
    }
};
#endif

class CudaTimer : public PerfTimer
{
public:
    void start()
    {
        CudaSafeCall(cudaDeviceSynchronize());
        PerfTimer::start();
    }

    void stop()
    {
        CudaSafeCall(cudaDeviceSynchronize());
        PerfTimer::stop();
    }

    void pause()
    {
        CudaSafeCall(cudaDeviceSynchronize());
        PerfTimer::pause();
    }

    double value()
    {
        return PerfTimer::value();
    }
};

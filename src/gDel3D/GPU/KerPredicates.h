
#pragma once

#include "../CommonTypes.h"
#include "HostToKernel.h"
#include "DPredWrapper.h"

#define PRED_THREADS_PER_BLOCK	32

void setPredWrapperConstant( const DPredWrapper &hostPredWrapper );

__global__ void kerInitPointLocationFast
(
int*    vertTetArr, 
Tet     tet,
int     tetIdx
)
;
__global__ void kerInitPointLocationExact
(
int*    vertTetArr, 
Tet     tet,
int     tetIdx
)
;
__global__ void
kerVoteForPoint
(
KerIntArray     vertexArr,
int*            vertexTetArr,
Tet*            tetArr,
int*            vertSphereArr,
int*            tetSphereArr,
InsertionRule   insRule
)
;
__global__ void
kerSplitPointsFast
(
KerIntArray vertexArr,
int*        vertexTetArr,
int*        tetToVert,
Tet*        tetArr,
char*       tetInfoArr,
KerIntArray freeArr
)
;
__global__ void
kerSplitPointsExactSoS
(
KerIntArray vertexArr,
int*        vertexTetArr,
int*        tetToVert,
Tet*        tetArr,
char*       tetInfoArr,
KerIntArray freeArr
)
;
__global__ void
kerCheckDelaunayFast
(
KerIntArray     actTetVec,
Tet*            tetArr,
TetOpp*         oppArr,
char*           tetInfoArr,
int*            tetVoteArr,
int*            voteArr,
int*            counterArr,
int             voteOffset
)
;
__global__ void
kerCheckDelaunayExact_Fast
(
KerIntArray     actTetVec,
Tet*            tetArr,
TetOpp*         oppArr,
char*           tetInfoArr,
int*            tetVoteArr,
int*            voteArr,
int2*           exactCheckVi, 
int*            counterArr,
int             voteOffset
)
;
__global__ void
kerCheckDelaunayExact_Exact
(
int*            actTetVec,
Tet*            tetArr,
TetOpp*         oppArr,
char*           tetInfoArr,
int*            tetVoteArr,
int*            voteArr,
int2*           exactCheckVi,
int*            counterArr,
int             voteOffset
)
;
__global__ void kerInitPredicate
( 
RealType* 
)
;
__global__ void
kerRelocatePointsFast
(
KerIntArray     vertexArr,
int*            vertexTetArr,
int*            tetToFlip,
FlipItem*       flipArr
)
;
__global__ void
kerRelocatePointsExact
(
KerIntArray     vertexArr,
int*            vertexTetArr,
int*            tetToFlip,
FlipItem*       flipArr
)
;

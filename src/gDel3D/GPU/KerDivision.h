
#pragma once

#include "../CommonTypes.h"
#include "HostToKernel.h"

__global__ void
kerMakeFirstTetra
(
Tet*    tetArr,
TetOpp* oppArr,
char*   tetInfoArr, 
Tet     tet,
int     tetIdx,
int     infIdx
)
;
__global__ void
kerSplitTetra
(
KerIntArray newVertVec,
KerIntArray insVertVec,
int*        vertArr, 
int*        vertTetArr,
int*        tetToVert,
Tet*        tetArr,
TetOpp*     oppArr,
char*       tetInfoArr,
int*        freeArr,
int*        vertFreeArr,
int         infIdx
)
;
__global__ void
kerMarkRejectedFlips
(
KerIntArray actTetArr,
TetOpp*     oppArr,
int*        tetVoteArr,
char*       tetInfoArr,
int*        voteArr,
int*        flipToTet,
int*        counterArr,
int         voteOffset
)
;
__global__ void
kerPickWinnerPoint
(
KerIntArray  vertexArr,
int*         vertexTetArr,
int*         vertSphereArr,
int*         tetSphereArr,
int*         tetVertArr
)
;
__global__ void
kerFlip
(
KerIntArray flipToTet,
Tet*        tetArr,
TetOpp*     oppArr,
char*       tetInfoArr,
int2*       tetMsgArr,
FlipItem*   flipArr,
int*        flip23NewSlot, 
int*        vertFreeArr, 
int*        freeArr,
int*        actTetArr,
KerIntArray insVertVec,
int         infIdx,
int         orgFlipNum
);
__global__ void
kerUpdateOpp
(
FlipItem*    flipVec,
TetOpp*      oppArr,
int2*        tetMsgArr,
int*         encodedFaceViArr,
int          orgFlipNum,
int          flipNum
); 
__global__ void
kerGatherFailedVerts
(
KerTetArray tetArr,
TetOpp*     tetOppArr,
int*        failVertArr,
int*        vertTetArr
)
;
__global__ void 
kerUpdateFlipTrace
(
FlipItem*   flipArr, 
int*        tetToFlip,
int         orgFlipNum, 
int         flipNum
)
;
__global__ void
kerMarkTetEmpty
(
KerCharArray tetInfoVec
)
;
__global__ void 
kerUpdateVertIdx
(
KerTetArray     tetVec,
int*            orgPointIdx
)
;
__global__ void 
kerMakeReverseMap
(
KerIntArray insVertVec, 
int*        scatterArr, 
int*        revMapArr,
int         num
)
;
__global__ void 
kerUpdateTetIdx
(
KerIntArray idxArr, 
int*        orderArr, 
int         oldInfBlockIdx, 
int         newInfBlockIdx
)
;
__global__ void 
kerUpdateBlockOppTetIdx
(
TetOpp* oppVec, 
int*    orderArr, 
int     oldInfBlockIdx, 
int     newInfBlockIdx,
int     oldTetNum
) 
;
__global__ void 
kerMarkSpecialTets
(
KerCharArray tetInfoVec, 
TetOpp*      oppArr
)
;
__global__ void 
kerUpdateVertFreeList
(
KerIntArray insTetVec, 
int*        vertFreeArr, 
int*        freeArr, 
int         startFreeIdx
)
;
__global__ void
kerNegateInsertedVerts
(
KerIntArray vertTetVec, 
int*        tetToVert
)
;
__global__ void 
kerAllocateFlip23Slot
(
KerIntArray flipToTet, 
Tet*        tetArr, 
int*        vertFreeArr, 
int*        freeArr, 
int*        flip23NewSlot,
int         infIdx,
int         tetNum
)
;
__global__ void 
kerShiftInfFreeIdx
(
int*    vertFreeArr, 
int*    freeArr, 
int     infIdx, 
int     start, 
int     shift
)
;
__global__ void 
kerUpdateBlockVertFreeList
(
KerIntArray insTetVec, 
int*        vertFreeArr, 
int*        freeArr, 
int*        scatterMap,
int         oldInsNum
)
;
__global__ void 
kerShiftOppTetIdx
(
TetOpp* oppArr, 
int     tetNum,
int     start,
int     shift
) 
;
__global__ void 
kerShiftTetIdx
(
KerIntArray idxVec, 
int         start,
int         shift
) 
;
__global__ void 
kerCollectFreeSlots
(
char* tetInfoArr, 
int*  prefixArr,
int*  freeArr,
int   newTetNum
)
;
__global__ void
kerMakeCompactMap
(
KerCharArray tetInfoVec, 
int*         prefixArr, 
int*         freeArr, 
int          newTetNum
)
;
__global__ void
kerCompactTets
(
KerCharArray tetInfoVec, 
int*         prefixArr, 
Tet*         tetArr, 
TetOpp*      oppArr, 
int          newTetNum
)
;


#pragma once

#include "CommonTypes.h"

#include "GPU/GPUDecl.h"
#include "GPU/DPredWrapper.h"

#include "CPU/Splaying.h"
#include "CPU/PredWrapper.h"

#include "PerfTimer.h"

class GpuDel
{
public:
	GpuDel();
	GpuDel( const GDelParams& params );
    ~GpuDel(); 
    
    void compute( const Point3HVec& input, GDelOutput *output );

private:
    // Execution configuration
    const GDelParams _params; 
    GDelOutput*      _output; 

    // Input
    Point3DVec  _pointVec; 

    // Output - Size proportional to tetNum
    TetDVec     _tetVec;            // Tetra list
    TetOppDVec  _oppVec;            // Opposite info
    CharDVec    _tetInfoVec;        // Tetra status (changed, deleted, etc.)

    // Supplemental arrays - Size proportional to tetNum
    IntDVec       _freeVec;         // List of free slots
    IntDVec       _actTetVec;       // List of active tetras
    Int2DVec      _tetMsgVec;       // Neighboring information when updating opps after flipping
    FlipDVec      _flipVec;         // Flip DAG

    std::vector<IntDVec*> _memPool;  // Memory pool, two items each of size TetMax

    // State
    bool        _doFlipping;        // To switch flipping on/off for some insertion round
    ActTetMode  _actTetMode;        // Compact/Collect mode to gather active tetras
    int         _insNum;            // Number of points inserted
    int         _voteOffset;        // To avoid reseting the voting array every round

    // Supplemental arrays - Size proportional to vertNum
    IntDVec       _orgPointIdx;     // Original point indices (used when sorting)
    IntDVec       _vertVec;         // List of remaining points
    IntDVec       _vertTetVec;      // Vertex location (tetra)
    IntDVec       _tetVoteVec;      // Vote tetras during flipping
    IntDVec       _vertSphereVec;   // Insphere value for voting during point insertion
    IntDVec       _vertFreeVec;     // Number of free slot per vertex
    IntDVec       _insVertVec;      // List of inserted vertices

    // Very small
	int			  _pointNum;        // Number of input points
    RealType      _minVal;          // Min and
    RealType      _maxVal;          //    max coordinate value
    IntHVec       _orgFlipNum;      // Number of flips in flipVec "before" each iteration
    IntDVec       _counterVec;      // Some device memory counters
    int           _maxTetNum;       // Maximum size of tetVec

    // Predicates
	Point3		  _ptInfty;         // The kernel point
    int           _infIdx;          // Kernel point index
    DPredWrapper  _dPredWrapper;    // Device predicate wrapper
	PredWrapper   _predWrapper;     // Host predicate wrapper

    // Star splaying
    Splaying      _splaying;          // Star splaying engine

    // Timing
    CudaTimer _profTimer; 

private:
    // Memory pool
    IntDVec &poolPopIntDVec();
    IntDVec &poolPeekIntDVec();
    void poolPushIntDVec( IntDVec &item );

    // Helpers
	void constructInitialTetra();
    void markSpecialTets();
    void expandTetraList( int newTetNum );
    void splitTetra();
    void doFlippingLoop( CheckDelaunayMode checkMode );
    bool doFlipping( CheckDelaunayMode checkMode );
    void dispatchCheckDelaunay( CheckDelaunayMode checkMode, IntDVec& voteVec ); 
    void compactTetras();
    void relocateAll();

    // Timing
    void startTiming(); 
    void pauseTiming(); 
    void stopTiming( double& accuTime ); 

    // Sorting
    void expandTetraList( IntDVec *newVertVec, int tailExtra, IntDVec *tetToVert, bool sort = false );

    template< typename T > 
    void reorderVec( IntDVec &orderVec, DevVector< T > &dataVec, int oldInfBlockIdx, int newInfBlockIdx, int size, T* init );

    template< typename T > 
    void pushVecTail( DevVector< T > &dataVec, int size, int from, int gap );

    // Main
    void initForFlip( const Point3HVec pointVec ); 
    void splitAndFlip();
    void outputToHost();
    void cleanup(); 

}; // class GpuDel

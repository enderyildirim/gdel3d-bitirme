//============================================================================
// Create on   : May 8, 2017
// Name        : ObjCreator.cpp
// Author      : Ender Yıldırım
// Mail        : enderyildirim@outlook.com
// Version     :
// Purpose     :
//============================================================================

#include <fstream>
#include <iomanip>

#include "ObjCreator.h"

#include "TriangleCreator.h"
#include "DelaunayChecker.h"

std::vector<std::string> ObjCreator::createObjFiles(const int pointNum,
		const int divide)
{
	std::vector<std::string> sectionFiles;

	const int section = (pointNum / divide);
	for (int i = 0; i < divide; ++i)
	{

		std::string sectionCount = NumberToString(i);
		std::string sectionName = "Bölüm" + sectionCount + ".obj";
		sectionFiles.push_back(sectionName);
		std::ofstream outputFile(sectionName.c_str());

		if (!outputFile.is_open())
		{
			std::cerr << "Dosya açılamadı!\n";
			std::exit(EXIT_FAILURE);
		}

		outputFile << COMMENT_KEYWORD << " ";
		outputFile << "Nokta aralığı = " << section*i << " - "
				   << section*(i + 1) << "\n";

		outputFile << COMMENT_KEYWORD << " ";
		outputFile << "Bölüm numarası = " << sectionCount << "\n";

		outputFile << COMMENT_KEYWORD << " ";
		outputFile << "Toplam bölüm sayısı = " << divide << "\n";

		outputFile << "#\n";

		outputFile.close();
	}

	return sectionFiles;
}

void ObjCreator::writeToObj(const std::string& filePath,
		const Point3HVec& pointVec, const GDelOutput& output)
{
	// Dosya .obj uzantılı mı?
	if(filePath.substr(filePath.length() - 4, filePath.length()) != ".obj")
	{
		std::cerr << "Dosya uzantısı .obj olmalı.\n";
		std::exit(EXIT_FAILURE);
	}

	std::fstream outputFile(filePath.c_str());
	if (!outputFile.is_open())
	{
		std::cerr << "Dosya açılamadı.\n";
		std::exit(EXIT_FAILURE);
	}

	// Yorum satırlarını atla
	for (int i = 0; i < 4; ++i)
	{
		std::string str;
		std::getline(outputFile, str);
	}

	const TetHVec& tetVec       = output.tetVec;
	const CharHVec& tetInfoVec  = output.tetInfoVec;

	std::cout << tetVec.size() << " adet tetrahedron vardır." << std::endl;

	for (int i = 0; i < tetVec.size(); ++i)
	{
		if ( !isTetAlive( tetInfoVec[i] ) ) continue;
		const Tet& tet = tetVec[ i ];

		for (int i = 0; i < TetFaceNum; ++i)
		{
			outputFile << VERTEX_KEYWORD << " ";
			// Tetrahedronun noktalar vektöründeki indisini kullanarak
			// noktayı dosyaya yazar.
			pointVec[tet._v[i]].print(outputFile);
			outputFile << "\n";
		}
	}


	/**
	 * Obj formatında face referans numarası 1'den başlar bu yüzden TetFace
	 * matrisindeki indislere tetrahedronun sayısına göre 1 eklemek gerekir.
	 * Örnek: 1. tetrahedron için yüzlerin aşağıdaki gibi
	 * oluşturulması gerekiyor.
	 * 		TetFace'deki  |    Obj formatına
	 * 			yüzler    |      göre
	 * 		{ 0, 1, 2 }  ->  { 1, 2, 3 }
     *		{ 0, 1, 3 }  ->  { 1, 2, 4 }
     *		{ 0, 2, 3 }  ->  { 1, 3, 4 }
     *		{ 1, 2, 3 }  ->  { 2, 3, 4 }
	 */
	long tetNum = 1; // Kaçıncı tetrahedronun yüzü olduğunu belirler.
	for (int i = 0; i < tetVec.size(); ++i)
	{
		for (int j = 0; j < TetFaceNum; ++j)
		{
			outputFile << FACE_KEYWORD << " ";
			outputFile << TetFace[j][0] + tetNum << " ";
			outputFile << TetFace[j][1] + tetNum << " ";
			outputFile << TetFace[j][2] + tetNum << " ";
			outputFile << "\n";
		}
		tetNum+=TetFaceNum;
	}

	outputFile.close();
}


#include <iomanip>
#include <fstream>

#include "gDel3D/GpuDelaunay.h"

#include "DelaunayChecker.h"
#include "InputCreator.h"
#include "gDel3D/CPU/CPUDecl.h"
#include "gDel3D/CommonTypes.h"

//eklenenler
#include "TriangleCreator.h"
#include "InputWriter.h"
#include "InputReader.h"
#include "ObjCreator.h"

const int deviceIdx     = 0; 
const int seed          = 123456789;
int pointNum     		= 1000000;  /// BU KADAR NOKTA
const Distribution dist = UniformDistribution;

// Forward declaration
void summarize( int pointNum, const GDelOutput& output );
void computeTriangulation(const Point3HVec& pointVec, GDelOutput *output);
const std::string getSectionName(int section);

int main( int argc, const char* argv[] ){  
	CudaSafeCall( cudaSetDevice( deviceIdx ) );  /// GPU seçilir
	CudaSafeCall( cudaDeviceReset() );     /// GPU resetlenir

	std::cout << "Generating input...\n";
	//InputCreator().makePoints( pointNum, dist, pointVec, seed ); // Rastgele nokta üretir

	// Dosyaya noktaları yazar
	InputWriter::writeToFile("outputPoints.txt", pointNum, 0, 1);

	double totalTime = 0.0;

	int divide = 5; // N adet nokta kaç bölüme ayrılacak
	int section = (pointNum / divide); // Her bölüm N / d kadar nokta içerecek
	std::vector<std::string> objFiles = ObjCreator::createObjFiles(pointNum, divide);
	for (int i = 0; i < divide; ++i)
	{
		Point3HVec   pointVec; // Noktaları tutan vektör
		GDelOutput   output; // Sonuç verisi

		// Dosyadan verilen aralıkta noktaları okur
		InputReader::readFromFile("outputPoints.txt", pointVec, section*i, section*(i + 1));
		std::cout << section*i << " ve " << section*(i + 1) << " arası okundu." << std::endl;

		// Delaunay üçgenlemesi hesaplanır
		computeTriangulation(pointVec, &output);

		// Bölümü hesaplamak için geçen süre toplam zamana eklenir.
		totalTime += output.stats.totalTime;

		ObjCreator::writeToObj(objFiles[i], pointVec, output);

		// GPU resetlenir
		CudaSafeCall( cudaDeviceReset() );
	}

	std::cout << "Total Time (ms) = " << totalTime << std::endl;

	return 0;
}

const std::string getSectionName(int section)
{
	return "Section" + section;
}

void computeTriangulation(const Point3HVec& pointVec, GDelOutput *output)
{
	std::cout << "Constructing 3D Delaunay triangulation...\n";

	// Optionally, some parameters can be specified. Here are the defaults:
	GDelParams params;
	params.insertAll    = false;
	params.insRule      = InsCircumcenter;
	params.noSorting    = false;
	params.noSplaying   = false;
	params.verbose      = false;
	//params.verbose      = true;

	GpuDel triangulator( params ); // ucgen olustur

	triangulator.compute( pointVec, output); /// TB 3genlestirme
}

void summarize( int pointNum, const GDelOutput& output ) 
{
	////
	// Summarize on screen
	////
	std::cout << std::endl;
	std::cout << "---- SUMMARY ----" << std::endl;
	std::cout << std::endl;

	std::cout << "PointNum       " << pointNum                                            << std::endl;
	std::cout << "FP Mode        " << (( sizeof( RealType ) == 8) ? "Double" : "Single")  << std::endl;
	std::cout << std::endl;
	std::cout << std::fixed << std::right << std::setprecision( 2 );
	std::cout << "TotalTime (ms) " << std::setw( 10 ) << output.stats.totalTime    << std::endl;
	std::cout << "InitTime       " << std::setw( 10 ) << output.stats.initTime     << std::endl;
	std::cout << "SplitTime      " << std::setw( 10 ) << output.stats.splitTime    << std::endl;
	std::cout << "FlipTime       " << std::setw( 10 ) << output.stats.flipTime     << std::endl;
	std::cout << "RelocateTime   " << std::setw( 10 ) << output.stats.relocateTime << std::endl;
	std::cout << "SortTime       " << std::setw( 10 ) << output.stats.sortTime     << std::endl;
	std::cout << "OutTime        " << std::setw( 10 ) << output.stats.outTime      << std::endl;
	std::cout << "SplayingTime   " << std::setw( 10 ) << output.stats.splayingTime << std::endl;
	std::cout << std::endl;
	std::cout << "# Flips        " << std::setw( 10 ) << output.stats.totalFlipNum << std::endl;
	std::cout << "# Failed verts " << std::setw( 10 ) << output.stats.failVertNum  << std::endl;
	std::cout << "# Final stars  " << std::setw( 10 ) << output.stats.finalStarNum << std::endl;
}

